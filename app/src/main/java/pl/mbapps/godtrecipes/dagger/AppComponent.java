package pl.mbapps.godtrecipes.dagger;

import javax.inject.Singleton;

import dagger.Component;
import pl.mbapps.godtrecipes.ui.RecipesListActivityComponent;
import pl.mbapps.godtrecipes.ui.RecipesListActivityModule;

@Singleton
@Component(
    modules = AppModule.class
)
public interface AppComponent {

    RecipesListActivityComponent add(RecipesListActivityModule module);

}

package pl.mbapps.godtrecipes.dagger;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.mbapps.godtrecipes.GodtRecipesApplication;
import pl.mbapps.godtrecipes.data.storage.RecipesStorage;
import pl.mbapps.godtrecipes.data.storage.RecipesStorageImpl;
import pl.mbapps.godtrecipes.realm.RealmManager;
import pl.mbapps.godtrecipes.retrofit.RetrofitProvider;
import pl.mbapps.godtrecipes.rx.RxTransformer;
import pl.mbapps.godtrecipes.service.RecipesService;
import pl.mbapps.godtrecipes.service.RecipesServiceApi;
import pl.mbapps.godtrecipes.service.RecipesServiceImpl;
import retrofit2.Retrofit;

@Module
public class AppModule {

    private GodtRecipesApplication application;

    public AppModule(GodtRecipesApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public RetrofitProvider provideRetrofitProvider() {
        return new RetrofitProvider(application);
    }

    @Provides
    @Singleton
    public RecipesServiceApi provideRecipesServiceApi(RetrofitProvider retrofitProvider) {
        Retrofit retrofit = retrofitProvider.getRetrofit();
        return retrofit.create(RecipesServiceApi.class);
    }

    @Provides
    @Singleton
    public RealmManager provideRealmManager() {
        return new RealmManager(application);
    }

    @Provides
    @Singleton
    public RecipesStorage provideRecipesStorage(RealmManager realmManager) {
        return new RecipesStorageImpl(realmManager);
    }

    @Provides
    @Singleton
    public RxTransformer provideRxTransformer() {
        return new RxTransformer();
    }

    @Provides
    @Singleton
    public RecipesService provideRecipesService(RecipesStorage recipesStorage, RecipesServiceApi recipesServiceApi, RxTransformer rxTransformer) {
        return new RecipesServiceImpl(recipesStorage, recipesServiceApi, rxTransformer);
    }

}

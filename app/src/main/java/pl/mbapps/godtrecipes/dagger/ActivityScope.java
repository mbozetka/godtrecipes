package pl.mbapps.godtrecipes.dagger;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}

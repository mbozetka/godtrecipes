package pl.mbapps.godtrecipes.data.storage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.realm.Realm;
import io.realm.RealmResults;
import pl.mbapps.godtrecipes.data.Recipe;
import pl.mbapps.godtrecipes.realm.RealmManager;

public class RecipesStorageImpl implements RecipesStorage {

    private RealmManager realmManager;

    public RecipesStorageImpl(RealmManager realmManager) {
        this.realmManager = realmManager;
    }

    @Override
    public boolean hasSavedRecipes() {
        Realm realm = realmManager.getDatabase();
        RealmResults<RecipeRealm> results = realm.where(RecipeRealm.class).findAll();
        return results != null && results.size() > 1;
    }

    @Override
    public Observable<List<Recipe>> getRecipes(final String query) {
        return Observable.create(new ObservableOnSubscribe<List<Recipe>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Recipe>> subscriber) throws Exception {
                List<Recipe> recipes = fetchRecipes(query);
                subscriber.onNext(recipes);
                subscriber.onComplete();
            }
        });
    }

    private List<Recipe> fetchRecipes(String query) {
        List<Recipe> recipes = loadRecipes();
        if (recipes != null && query != "") {
            return filterRecipes(recipes, query);
        }
        return recipes;
    }

    private List<Recipe> loadRecipes() {
        Realm realm = realmManager.getDatabase();
        RealmResults<RecipeRealm> results = realm.where(RecipeRealm.class).findAll();
        if (results == null || results.size() < 1) {
            return null;
        }
        List<Recipe> recipes = new ArrayList<>();
        for (RecipeRealm recipeRealm : results) {
            recipes.add(recipeRealm.toModel());
        }
        return recipes;
    }

    private List<Recipe> filterRecipes(List<Recipe> recipes, String query) {
        List<Recipe> filteredRecipes = new ArrayList<>();
        for (Recipe recipe : recipes) {
            if (shouldAddRecipe(recipe, query)) {
                filteredRecipes.add(recipe);
            }
        }
        return filteredRecipes;
    }

    private boolean shouldAddRecipe(Recipe recipe, String query) {
        if (recipe.title.toLowerCase().contains(query.toLowerCase())) {
            return true;
        }
        if (recipe.ingredients == null) {
            return false;
        }
        for (Recipe.RecipeIngredient ingredient : recipe.ingredients) {
            if (ingredientContainsGivenQuery(ingredient, query)) {
                return true;
            }
        }
        return false;
    }

    private boolean ingredientContainsGivenQuery(Recipe.RecipeIngredient ingredient, String query) {
        return ingredient.name != null && ingredient.name.toLowerCase().contains(query.toLowerCase());
    }

    @Override
    public void saveRecipes(List<Recipe> recipes) {
        final List<RecipeRealm> recipesRealm = new ArrayList<>();
        for (Recipe recipe : recipes) {
            recipesRealm.add(RecipeRealm.toPersistent(recipe));
        }
        Realm realm = realmManager.getDatabase();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(recipesRealm);
            }
        });
        realm.close();
    }

}

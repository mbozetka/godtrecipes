package pl.mbapps.godtrecipes.data.storage;

import java.util.List;

import io.reactivex.Observable;
import pl.mbapps.godtrecipes.data.Recipe;

public interface RecipesStorage {

    boolean hasSavedRecipes();

    Observable<List<Recipe>> getRecipes(String query);

    void saveRecipes(List<Recipe> recipes);

}
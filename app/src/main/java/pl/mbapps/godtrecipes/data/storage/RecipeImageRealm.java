package pl.mbapps.godtrecipes.data.storage;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pl.mbapps.godtrecipes.data.Recipe;

public class RecipeImageRealm extends RealmObject {

    @PrimaryKey
    public String imboId;
    public String url;

    public static RecipeImageRealm toPersistent(Recipe.RecipeImage image) {
        RecipeImageRealm imageRealm = new RecipeImageRealm();
        imageRealm.imboId = image.imboId;
        imageRealm.url = image.url;
        return imageRealm;
    }

    public Recipe.RecipeImage toModel() {
        Recipe.RecipeImage image = new Recipe.RecipeImage();
        image.imboId = imboId;
        image.url = url;
        return image;
    }

}
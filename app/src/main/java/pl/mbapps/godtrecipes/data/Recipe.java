package pl.mbapps.godtrecipes.data;

import java.util.List;

public class Recipe {

    public int id;
    public String title;
    public String description;
    public List<RecipeImage> images;
    public List<RecipeIngredient> ingredients;

    public String getImageUrl() {
        if (images == null || images.size() < 1) {
            return null;
        }
        return images.get(0).url;
    }

    public static class RecipeImage {
        public String imboId;
        public String url;
    }

    public static class RecipeIngredient {
        public int id;
        public String name;
    }

}

package pl.mbapps.godtrecipes.data.storage;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pl.mbapps.godtrecipes.data.Recipe;

public class RecipeIngredientRealm extends RealmObject {

    @PrimaryKey
    public int id;
    public String name;

    public static RecipeIngredientRealm toPersistent(Recipe.RecipeIngredient ingredient) {
        RecipeIngredientRealm ingredientRealm = new RecipeIngredientRealm();
        ingredientRealm.id = ingredient.id;
        ingredientRealm.name = ingredient.name;
        return ingredientRealm;
    }

    public Recipe.RecipeIngredient toModel() {
        Recipe.RecipeIngredient ingredient = new Recipe.RecipeIngredient();
        ingredient.name = name;
        return ingredient;
    }

}

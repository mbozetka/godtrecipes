package pl.mbapps.godtrecipes.data.storage;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import pl.mbapps.godtrecipes.data.Recipe;

public class RecipeRealm extends RealmObject {

    public int id;
    public String title;
    public String description;
    public RealmList<RecipeImageRealm> images;
    public RealmList<RecipeIngredientRealm> ingredients;

    public static RecipeRealm toPersistent(Recipe recipe) {
        RecipeRealm recipeRealm = new RecipeRealm();
        recipeRealm.id = recipe.id;
        recipeRealm.title = recipe.title;
        recipeRealm.description = recipe.description;
        if (recipe.images != null) {
            recipeRealm.images = new RealmList<>();
            for (Recipe.RecipeImage image : recipe.images) {
                RecipeImageRealm imageRealm = RecipeImageRealm.toPersistent(image);
                recipeRealm.images.add(imageRealm);
            }
        }
        if (recipe.ingredients != null) {
            recipeRealm.ingredients = new RealmList<>();
            for (Recipe.RecipeIngredient ingredient : recipe.ingredients) {
                RecipeIngredientRealm ingredientRealm = RecipeIngredientRealm.toPersistent(ingredient);
                recipeRealm.ingredients.add(ingredientRealm);
            }
        }
        return recipeRealm;
    }

    public Recipe toModel() {
        Recipe recipe = new Recipe();
        recipe.id = id;
        recipe.title = title;
        recipe.description = description;
        if (images != null) {
            recipe.images = new ArrayList<>();
            for (RecipeImageRealm imageRealm : images) {
                recipe.images.add(imageRealm.toModel());
            }
        }
        if (ingredients != null) {
            recipe.ingredients = new ArrayList<>();
            for (RecipeIngredientRealm ingredientRealm : ingredients) {
                recipe.ingredients.add(ingredientRealm.toModel());
            }
        }
        return recipe;
    }

}

package pl.mbapps.godtrecipes.realm;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import pl.mbapps.godtrecipes.GodtRecipesApplication;

public class RealmManager {

    private static String DATABASE_NAME = "Recipes.db";
    private static int DATABASE_VERSION = 1;
    private GodtRecipesApplication application;
    private boolean initialized;

    public RealmManager(GodtRecipesApplication application) {
        this.application = application;
    }

    public Realm getDatabase() {
        if (!initialized) {
            initDatabase();
        }
        return Realm.getDefaultInstance();
    }

    private void initDatabase() {
        Realm.init(application);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
            .name(DATABASE_NAME)
            .schemaVersion(DATABASE_VERSION)
            .deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(configuration);
        initialized = true;
    }

}
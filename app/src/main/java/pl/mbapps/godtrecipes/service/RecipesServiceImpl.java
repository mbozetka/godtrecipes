package pl.mbapps.godtrecipes.service;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import pl.mbapps.godtrecipes.data.Recipe;
import pl.mbapps.godtrecipes.data.storage.RecipesStorage;
import pl.mbapps.godtrecipes.rx.RxTransformer;

public class RecipesServiceImpl implements RecipesService {

    private RecipesStorage storage;
    private RecipesServiceApi apiService;
    private RxTransformer rxTransformer;

    public RecipesServiceImpl(RecipesStorage storage, RecipesServiceApi apiService, RxTransformer rxTransformer) {
        this.storage = storage;
        this.apiService = apiService;
        this.rxTransformer = rxTransformer;
    }

    @Override
    public Observable<List<Recipe>> getRecipes(final String query) {
        if (storage.hasSavedRecipes()) {
            return storage.getRecipes(query);
        }

        return apiService.getRecipes("", "thumbnail-medium", 1, 50, 0)
            .flatMap(new Function<List<Recipe>, ObservableSource<List<Recipe>>>() {
                @Override
                public ObservableSource<List<Recipe>> apply(List<Recipe> recipes) throws Exception {
                    storage.saveRecipes(recipes);
                    return storage.getRecipes(query);
                }
            })
            .compose(rxTransformer.<List<Recipe>>subscribeOnIo())
            .compose(rxTransformer.<List<Recipe>>observeOnMain());
    }

}

package pl.mbapps.godtrecipes.service;

import java.util.List;

import io.reactivex.Observable;
import pl.mbapps.godtrecipes.data.Recipe;

public interface RecipesService {

    Observable<List<Recipe>> getRecipes(String query);

}

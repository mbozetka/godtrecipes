package pl.mbapps.godtrecipes.service;

import java.util.List;

import io.reactivex.Observable;
import pl.mbapps.godtrecipes.data.Recipe;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RecipesServiceApi {

    @GET("getRecipesListDetailed")
    Observable<List<Recipe>> getRecipes(@Query("tags") String tags, @Query("size") String thumbnailSize,
                                        @Query("ration") int ratio, @Query("limit") int limit, @Query("from") int from);

}
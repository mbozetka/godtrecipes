package pl.mbapps.godtrecipes.retrofit;

import android.content.Context;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.mbapps.godtrecipes.R;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider {

    private Retrofit retrofit;
    private Context context;

    public RetrofitProvider(Context context) {
        this.context = context;
    }

    private void initRetrofit() {
        Retrofit.Builder builder = new Retrofit.Builder();

        builder.baseUrl(context.getString(R.string.api_endpoint));
        builder.addConverterFactory(GsonConverterFactory.create());
        builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(prepareLoggingInterceptor())
            .build();
        
        builder.client(client);

        retrofit = builder.build();
    }

    private Interceptor prepareLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    public Retrofit getRetrofit() {
        if (retrofit == null) {
            initRetrofit();
        }
        return retrofit;
    }

}
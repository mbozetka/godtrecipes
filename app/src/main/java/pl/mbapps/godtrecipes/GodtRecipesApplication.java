package pl.mbapps.godtrecipes;

import android.app.Application;
import android.content.Context;

import pl.mbapps.godtrecipes.dagger.AppComponent;
import pl.mbapps.godtrecipes.dagger.AppModule;
import pl.mbapps.godtrecipes.dagger.DaggerAppComponent;
import timber.log.Timber;

public class GodtRecipesApplication extends Application {

    private AppComponent appComponent;

    public static GodtRecipesApplication get(Context context) {
        return (GodtRecipesApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initDagger();
        Timber.plant(new Timber.DebugTree());
    }

    private void initDagger() {
        appComponent = DaggerAppComponent.builder()
            .appModule(new AppModule(this))
            .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}

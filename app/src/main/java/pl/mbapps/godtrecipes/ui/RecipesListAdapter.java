package pl.mbapps.godtrecipes.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.mbapps.godtrecipes.R;
import pl.mbapps.godtrecipes.data.Recipe;

public class RecipesListAdapter extends RecyclerView.Adapter<RecipesListAdapter.RecipeViewHolder> {

    private List<Recipe> recipes;
    private RecipesListActivity activity;

    public RecipesListAdapter(RecipesListActivity activity) {
        this.activity = activity;
        recipes = new ArrayList<>();
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View recipeView = LayoutInflater.from(activity).inflate(R.layout.view_recipe, parent, false);
        return new RecipeViewHolder(recipeView, activity);
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder holder, int position) {
        Recipe recipe = recipes.get(position);
        holder.fillWithRecipe(recipe);
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public void updateRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
        notifyDataSetChanged();
    }

    public static class RecipeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_recipe_img)
        ImageView image;
        @BindView(R.id.tv_title)
        TextView title;
        @BindView(R.id.tv_description)
        TextView description;
        @BindView(R.id.tv_ingredients)
        TextView ingredients;

        Context context;

        public RecipeViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            ButterKnife.bind(this, itemView);
        }

        public void fillWithRecipe(Recipe recipe) {
            Picasso.with(context).load(recipe.getImageUrl()).placeholder(R.drawable.placeholder).into(image);
            title.setText(recipe.title);
            description.setText(acknowledgeHtml(recipe.description));
            ingredients.setText(getRecipeIngredientsStr(recipe));
        }

        private String getRecipeIngredientsStr(Recipe recipe) {
            if (recipe.ingredients == null || recipe.ingredients.size() < 1) {
                return "";
            }

            StringBuilder sb = new StringBuilder();

            for (Recipe.RecipeIngredient ingredient : recipe.ingredients) {
                if (ingredient.name == null) {
                    continue;
                }
                String trimmedIngredient = ingredient.name.trim();
                if (trimmedIngredient.length() < 1) {
                    continue;
                }
                sb.append(ingredient.name);
                sb.append(", ");
            }

            if (sb.length() > 2) {
                sb.delete(sb.length() - 2, sb.length());
            }

            return sb.toString();
        }

        public Spanned acknowledgeHtml(String html) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
            }
            return Html.fromHtml(html);
        }

    }

}

package pl.mbapps.godtrecipes.ui;

import java.util.List;

import io.reactivex.observers.DisposableObserver;
import pl.mbapps.godtrecipes.R;
import pl.mbapps.godtrecipes.data.Recipe;
import pl.mbapps.godtrecipes.service.RecipesService;
import timber.log.Timber;

public class RecipesListActivityPresenter {

    private RecipesListActivity activity;
    private RecipesService service;

    public RecipesListActivityPresenter(RecipesListActivity activity, RecipesService service) {
        this.activity = activity;
        this.service = service;
    }

    public void updateRecipesForQuery(String query) {
        service.getRecipes(query).subscribe(new DisposableObserver<List<Recipe>>() {
            @Override
            public void onNext(List<Recipe> recipes) {
                if (recipes.size() > 0) {
                    activity.updateRecipes(recipes);
                    return;
                }
                activity.showNoDataInfo(R.string.no_data_for_given_criteria);
            }

            @Override
            public void onError(Throwable e) {
                activity.showNoDataInfo(R.string.fetch_data_problem);
                Timber.e("Problems with updating recipes: " + e);
            }

            @Override
            public void onComplete() {

            }
        });
    }

}

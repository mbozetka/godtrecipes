package pl.mbapps.godtrecipes.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.mbapps.godtrecipes.GodtRecipesApplication;
import pl.mbapps.godtrecipes.R;
import pl.mbapps.godtrecipes.data.Recipe;

public class RecipesListActivity extends Activity {

    @Inject
    RecipesListActivityPresenter presenter;
    @Inject
    LinearLayoutManager recipesLayoutManager;
    @Inject
    RecipesListAdapter recipesListAdapter;

    @BindView(R.id.sr_layout)
    SwipeRefreshLayout srLayout;
    @BindView(R.id.rv_recipes)
    RecyclerView rvRecipes;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.sv_recipes_search)
    SearchView searchView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recipes);
        ButterKnife.bind(this);
        setupDependencies();
        initRecipesList();

        srLayout.setRefreshing(true);
        presenter.updateRecipesForQuery("");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                presenter.updateRecipesForQuery(query);
                return false;
            }
        });
        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.updateRecipesForQuery(searchView.getQuery().toString());
            }
        });
    }

    private void setupDependencies() {
        GodtRecipesApplication.get(this)
            .getAppComponent()
            .add(new RecipesListActivityModule(this))
            .inject(this);
    }

    private void initRecipesList() {
        rvRecipes.setLayoutManager(recipesLayoutManager);
        rvRecipes.setAdapter(recipesListAdapter);
    }

    public void updateRecipes(List<Recipe> recipes) {
        recipesListAdapter.updateRecipes(recipes);
        tvNoData.setVisibility(View.GONE);
        rvRecipes.setVisibility(View.VISIBLE);
        srLayout.setRefreshing(false);
    }

    public void showNoDataInfo(int resId) {
        rvRecipes.setVisibility(View.GONE);
        tvNoData.setText(resId);
        tvNoData.setVisibility(View.VISIBLE);
        srLayout.setRefreshing(false);
    }

}

package pl.mbapps.godtrecipes.ui;

import android.support.v7.widget.LinearLayoutManager;

import dagger.Module;
import dagger.Provides;
import pl.mbapps.godtrecipes.dagger.ActivityScope;
import pl.mbapps.godtrecipes.service.RecipesService;

@Module
public class RecipesListActivityModule {

    private RecipesListActivity activity;

    public RecipesListActivityModule(RecipesListActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    RecipesListActivity provideRecipesActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    RecipesListActivityPresenter provideRecipesActivityPresenter(RecipesService service) {
        return new RecipesListActivityPresenter(activity, service);
    }

    @Provides
    @ActivityScope
    LinearLayoutManager provideRecipesLayoutManager() {
        return new LinearLayoutManager(activity);
    }

    @Provides
    RecipesListAdapter provideRecipesAdapter() {
        return new RecipesListAdapter(activity);
    }

}
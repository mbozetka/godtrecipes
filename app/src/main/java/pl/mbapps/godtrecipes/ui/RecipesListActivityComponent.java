package pl.mbapps.godtrecipes.ui;

import dagger.Subcomponent;
import pl.mbapps.godtrecipes.dagger.ActivityScope;

@ActivityScope
@Subcomponent(
    modules = RecipesListActivityModule.class
)
public interface RecipesListActivityComponent {

    RecipesListActivity inject(RecipesListActivity activity);

}
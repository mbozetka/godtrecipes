package pl.mbapps.godtrecipes.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import pl.mbapps.godtrecipes.data.Recipe;
import pl.mbapps.godtrecipes.data.storage.RecipesStorage;
import pl.mbapps.godtrecipes.rx.MockRxTransformer;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RecipesServiceImplTests {

    @Mock
    RecipesStorage recipesStorageMock;
    @Mock
    RecipesServiceApi recipesServiceApiMock;

    private RecipesServiceImpl service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new RecipesServiceImpl(recipesStorageMock, recipesServiceApiMock, new MockRxTransformer());
    }

    @Test
    public void test_ShouldGetRecipesFromStorage_WhenStorageHasRecipes() {
        when(recipesStorageMock.hasSavedRecipes()).thenReturn(true);
        when(recipesStorageMock.getRecipes(anyString())).thenReturn(Observable.<List<Recipe>>empty());

        service.getRecipes("some query");

        verify(recipesStorageMock).hasSavedRecipes();
        verify(recipesStorageMock).getRecipes("some query");
        verify(recipesServiceApiMock, never()).getRecipes(anyString(), anyString(), anyInt(), anyInt(), anyInt());
    }

    @Test
    public void test_ShouldGetRecipesFromService_WhenStorageEmpty() {
        when(recipesStorageMock.hasSavedRecipes()).thenReturn(false);
        when(recipesServiceApiMock.getRecipes(anyString(), anyString(), anyInt(), anyInt(), anyInt())).thenReturn(Observable.<List<Recipe>>empty());

        service.getRecipes("some query");

        verify(recipesStorageMock).hasSavedRecipes();
        verify(recipesStorageMock, never()).getRecipes("some query");
        verify(recipesServiceApiMock).getRecipes(anyString(), anyString(), anyInt(), anyInt(), anyInt());
    }

    @Test
    public void test_ShouldSaveRecipes_WhenGetRecipesFromService() {
        List<Recipe> recipeList = new ArrayList<>();
        when(recipesStorageMock.hasSavedRecipes()).thenReturn(false);
        when(recipesServiceApiMock.getRecipes(anyString(), anyString(), anyInt(), anyInt(), anyInt())).thenReturn(Observable.just(recipeList));

        service.getRecipes("some query").subscribe(new DisposableObserver<List<Recipe>>() {
            @Override
            public void onNext(List<Recipe> recipes) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                verify(recipesStorageMock).hasSavedRecipes();
                verify(recipesStorageMock, never()).getRecipes("some query");
                verify(recipesServiceApiMock).getRecipes(anyString(), anyString(), anyInt(), anyInt(), anyInt());
            }
        });
    }
}

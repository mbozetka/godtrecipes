package pl.mbapps.godtrecipes.ui;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import pl.mbapps.godtrecipes.R;
import pl.mbapps.godtrecipes.data.Recipe;
import pl.mbapps.godtrecipes.service.RecipesService;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RecipesListActivityPresenterTests {

    @Mock
    RecipesService recipesServiceMock;
    @Mock
    RecipesListActivity recipesListActivityMock;

    RecipesListActivityPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new RecipesListActivityPresenter(recipesListActivityMock, recipesServiceMock);
    }

    @Test
    public void test_ShouldShowDataFetchProblem_WhenServiceReturnedError() {
        when(recipesServiceMock.getRecipes(anyString())).thenReturn(Observable.<List<Recipe>>error(new NullPointerException()));

        presenter.updateRecipesForQuery("some query");

        verify(recipesListActivityMock).showNoDataInfo(R.string.fetch_data_problem);
        verify(recipesListActivityMock, never()).updateRecipes(ArgumentMatchers.<Recipe>anyList());
    }

    @Test
    public void test_ShouldShowNoDataForCriteria_WhenServiceResponseIsEmpty() {
        when(recipesServiceMock.getRecipes(anyString())).thenReturn(Observable.<List<Recipe>>just(new ArrayList<Recipe>()));

        presenter.updateRecipesForQuery("some query");

        verify(recipesListActivityMock).showNoDataInfo(R.string.no_data_for_given_criteria);
        verify(recipesListActivityMock, never()).updateRecipes(ArgumentMatchers.<Recipe>anyList());
    }

    @Test
    public void test_ShouldShowRecipes_WhenServiceReturnedSome() {
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(new Recipe());
        when(recipesServiceMock.getRecipes(anyString())).thenReturn(Observable.just(recipes));

        presenter.updateRecipesForQuery("some query");

        verify(recipesListActivityMock, never()).showNoDataInfo(anyInt());
        verify(recipesListActivityMock).updateRecipes(recipes);
    }
}

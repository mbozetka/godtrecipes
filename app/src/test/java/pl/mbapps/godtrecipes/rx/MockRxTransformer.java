package pl.mbapps.godtrecipes.rx;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;

public class MockRxTransformer extends RxTransformer {

    @Override
    public <T> ObservableTransformer<T, T> subscribeOnIo() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream; // do nothing
            }
        };
    }

    @Override
    public <T> ObservableTransformer<T, T> observeOnMain() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream;
            }
        };
    }

}
